                             ..';::::;,'..                             
                       .:dOXMMNX00OOO0KXNMMN0xc'                       
                   .lOWWKxc,.     ...     .':d0WMKo'                   
                .oNMKo'  .,lx0XWMMMMMMMWX0ko;.  .l0MWx'                
              :KMXl. .:kNMXko:'..     ..';lkXMWOc.  cKMX:              
            :XMO'  cKMNd;.  ':oxO0KKK0Oxoc'.  ,oXMXl. .kMN:            
          'KM0. .xWWx'  ;dXMW0xl:;,,,;:ldONMNx:  .dWMx. .0MK.          
         lMW:  dMNl  ,kWWk:.  .,:loool:,.  .;xNMO,  cNMd  :WWc         
        kMK. ,NMd  ;KM0:  'o0WMXOxdddxOKWMKd,  ;0MX;  xMX' .XMd        
       OM0  lMN' .0M0' .oNM0l'   .'''..  .:OWNd. 'KM0. ,WM: .XMd       
      xMK  oMX. ;WWc  dWWo. .lONMNXKXNMW0o.  lWWo  lMN' .WM: .NMc      
     ,MW. :MW. ;MW' .XMx  ,0MKl'       'lKMK,  kMK. :MW. ,MW. ;MW.     
     KMd  NMc .WM: .NMl  OMK'  ckXWMWXOc. 'KMk  xMX  dMX  xMO  KMo     
    .MM. :MN  xMO  KMx  KMk  lWWx,...,dWWl  0Mk  KMd  WM; 'MM. lMX     
    :MN  kMx  NM; 'MM. lMX  dMK. 'xOx, .NMc .MM. cMN  kMx  WM; ,MM     
    oM0  KMl .MM. lMX  0Md  MM, ,MMNko  OMd  MM, ;MM  xMk  WM: 'MM.    
    oM0  KMl .MM. cMX  OMx  WM: .WMo..;OMK. cMN  dMK  KMo .MM' ;MW     
    ;MW  xMk  XMc .MM. ;MW. cMN' .xXNNKx;  oMN' 'WM, 'MM. :MW  xMO     
    .WM, ,MW. lMX  kM0  xMK. ;NMk,     .,dNWx. ,WMc  XMd  XMo  WM:     
     xM0  0Mx  KMo  KMk  oMN:  ;kNMWNNMMNkc  .kMX, .KMk  kMK  xMK      
     .WM: .WM: .NMc  0M0. .OMNo'   ....   'oKMKc  cWMo  kMX. :MW.      
      ,MW' 'WM; .KMd  cNMx. .l0WMXOkxxk0XMNOl. .lNMO. .KM0. :MW,       
       :MW. .NMl  dMX;  cXMKl.  .,:cccc;'.  'l0MWx. .xMWc  dMN.        
        ;WW;  OM0. .OMXc  .l0WW0xoc  ':ldkKMMXx;  'kMWd. ,XMO.         
         .XMx  ;XMx. .oNWOc.  ,0MMW  xMMMWd,.  ,xNMXl  '0MX;           
           oWNc  :XMO;  'o0WNo  kMW  xMMO  ,xKMMKo.  cKMX:             
            .xMXc  'xWWk:.  WM, ,MW  xMW  oMWl,  .cOWWO,               
              .dWWx,  'o0WWWWd  xMk  XMN  dMNcckNMWk:                  
                 ;kWWk:.  ..  'OMO  oMMMx  :kKKkc.                     
                    ,o0MN0kk0WWO,  .:::::,                             
                        .;::;.               

What is Chakravyuh ?
####################

Chakravyuh is a simple, bare-bones security microprocessor to demonstrate
the ideas of authentication, asymmetric-key cryptography using the celebrated 
RSA algorithm as well as secure on-ship storage of persistent data. 
The motivation for designing this security processor was to attempt the design 
and implementation of an in-house embedded RoT (Root-Of-Trust) which could be 
used in India on commercial security systems like CAS 
(Conditional Access Systems) used in broadcast television applications.

The makers-in-mischief claim no novelty of the ideas presented, this was simply 
a means to do some interesting systems design work using HDL & have fun in 
the whole process !

References :
############

[1] Jai Skand Tripathi, Priya Keerti Tripathi, & Deepti Shakti
Tripathi, An Efficient Design of Vedic Multiplier using
New Encoding Scheme, International Journal of
Computer Applications (0975 – 8887) Volume 53– No.11,
September 2012

[2] Jinyoung Moon, Jongyoul Park, and Euihyun Paik,
JavaCard-based Two-Level User Key Management ,
Home Network Group, Digital Home Research Division,
Electronics and Telecommunication Research Institute,
161 Gajeong-dong, Yuseong-gu, Daejeon, Korea.

[3] Dr. K.S. Gurumurthy, M.S Prahalad, Fast and Power
Efficient 16×16 Array of Array Multiplier using Vedic
Multiplication, DOS in Electronics and Communication
Engineering, UVCE Bangalore

[4]Dani George, Bonifus P.L., RSA Encryption System Using
Encoded Multiplier and Vedic Mathematics, 2013
International Conference on Advanced Computing and
Communication Systems (ICACCS -2013), Dec. 19 – 21,
2013, Coimbatore, INDIA

[5]Garima Rawat, Khyati Rathore, Siddharth Goyal, Shefali
Kala and Poornima Mittal, Design and Analysis of ALU:
Vedic Mathematics Approach, International Conference
on Computing, Communication and Automation (ICCCA2015)

[6] The Search Security Website
http://searchsecurity.techtarget.com/definition/RSA

[7] The book “Computer Networks : A Top Down approach
with Networking” : http://netlab.ulusofona.pt/rc/book/7-security/7_03/index.htm
